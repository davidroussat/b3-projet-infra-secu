# B3- Projet Infra - Secu

## Introduction

L'infrastructure que nous avons imaginé peut être déployée à l'identique en suivant les consignes qui vont suivre.
Il convient de prendre en compte que certaines acitons ne peuvent pas être automatisées.

Voici la liste des actions nécessitant une interaction:
    - Obtention d'un certificat => Demande de mot de passe
    - Installation de Passbolt server => Mode intéractif
    - Joindre les serveurs au domaine => Demande de mot de passe

## Installation

Pour le bon fonctionnement de l'environnement proposé, il convient de respecter les étapes suivantes:

1- Installer:
- 4 VM Ubuntu 22.4 avec: 2048Go RAM / 2 PU / 20Gio de stockage 
- 1 VM RockyLinux avec: 2048Go RAM / 2 PU / 20Gio de stockage 
- 1 Windows Server avec 4096Go RAM / 2 PU / 20Gio de stockage 

2- Connectez vous sur chacunes des VM en ROOT pour les linux et en Admin de domaine pour Windows

3- Sur chacunes des VM, cloner le repo "https://gitlab.com/davidroussat/b3-projet-infra-secu"

4- Copier:
- le dossier WIndows Server sur la VM WIndows
- le dossier vpn_strongswan sur la VM RockyLinux
- répartisser les dossiers restants sur les 4 autres VM

5- Pour chacunes des VM, placer vous dans le dossier que vous venez de coller, puis suivez les README.txt relatif à chaque technologie afin de déployer l'infrastructure en respectant l'ordre suivant:
- Le Windows server
- la PKI
- le VPN
- Le serveur Mail
- Passbolt
- le Bastion


    

