# Il est primordial d'effectuer ces étapes dans l'ordre:

1 - Lancer le script BEFORE_INSTALLATION.sh en tant que root


2 - Installer la root_ca dans le Truststore:

	step certificate install $(step path)/certs/root_ca.crt


3- Génération du certificat pour la coonnxion VPN:
	step ca certificate WindowsServer.projet.lab vpn.crt vpn.key 
	step certificate p12 vpn.pfx vpn.crt vpn.key
		Nb: Un mot de passe vous sera demander afin de protéger la clé privée

	Nb: vpn.pfx et la root_ca.crt sont à envoyer sur le WIndows_Server à l'emplacement suivant:
		"C:\Users\Administrator\Desktop\"


3 - Lancer le script installation.sh

4 - Pour joindre le serveur au domain:
	a- Taper la commande: 
		realm join -U Administrator projet.lab
	Un mot de passe vous sera demander:	
		"azertyuiop"
	
	b- Executer le script joind_ad.sh
