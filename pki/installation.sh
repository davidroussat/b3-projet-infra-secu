#!/bin/bash

# PKI Installation by David R.

set -euo pipefail
IFS=$'\n\t'

# Network configuration
nmcli conn add con-name privee ifname enp0s8 ipv4.address 10.7.0.14/24 ipv4.gateway 10.7.0.254  ipv4.method manual type ethernet connection.autoconnect yes

nmcli conn reload
nmcli conn up enp0s8

# Hostname configuration
hostnamectl set-hostname pki.projet.lab

apt update -y 1>/dev/null
tee -a /etc/apt/sources.list <<EOF
deb http://us.archive.ubuntu.com/ubuntu/ bionic universe
deb http://us.archive.ubuntu.com/ubuntu/ bionic-updates universe
EOF
apt -y install realmd libnss-sss libpam-sss sssd sssd-tools adcli samba-common-bin oddjob oddjob-mkhomedir packagekit


# Step-cli
wget https://dl.step.sm/gh-release/cli/docs-cli-install/v0.23.4/step-cli_0.23.4_amd64.deb
dpkg -i step-cli_0.23.4_amd64.deb

# Step ca
wget https://dl.step.sm/gh-release/certificates/docs-ca-install/v0.23.2/step-ca_0.23.2_amd64.deb
dpkg -i step-ca_0.23.2_amd64.deb

# Files configuration
cp -r step/* /home/david/.step/
chown -R david:david /home/david/.step

# Create user service
mkdir -p /home/david/.config/systemd/user/
cp step.service /home/david/.config/systemd/user/
chown -R david:david /home/david/.config/

# Disable UFW firewall
systemctl disable ufw.service
systemctl stop ufw.service
systemctl mask ufw.service

# Nftables installation
apt install nftables -y 1>/dev/null
cp nftables.conf /etc/ 
systemctl enable --now nftables.service

# Switch user 
su - david
systemctl --user daemon-reload
systemctl --user start step.service
systemctl --user enable step.service



