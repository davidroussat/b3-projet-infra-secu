#!/bin/bash

# PKI Installation by David R.

set -euo pipefail
IFS=$'\n\t'

# Network configuration
nmcli conn add con-name privee ifname enp0s8 ipv4.address 10.7.0.13/24 ipv4.gateway 10.7.0.254  ipv4.method manual type ethernet connection.autoconnect yes

nmcli conn reload
nmcli conn up enp0s8

# Hostname configuration
hostnamectl set-hostname passbolt.projet.lab

# Package installations
apt update -y 1>/dev/null

tee -a /etc/apt/sources.list <<EOF
deb http://us.archive.ubuntu.com/ubuntu/ bionic universe
deb http://us.archive.ubuntu.com/ubuntu/ bionic-updates universe
EOF
apt -y install realmd libnss-sss libpam-sss sssd sssd-tools adcli samba-common-bin oddjob oddjob-mkhomedir packagekit 1>/dev/null

# Install dependencies to passbolt server
wget "https://download.passbolt.com/ce/installer/passbolt-repo-setup.ce.sh"
wget https://github.com/passbolt/passbolt-dep-scripts/releases/latest/download/passbolt-ce-SHA512SUM.txt
sha512sum -c passbolt-ce-SHA512SUM.txt && sudo bash ./passbolt-repo-setup.ce.sh  || echo \"Bad checksum. Aborting\" && rm -f passbolt-repo-setup.ce.sh


# Disable UFW firewall
systemctl disable ufw.service
systemctl stop ufw.service
systemctl mask ufw.service

# Nftables installation
apt install nftables -y 1>/dev/null
cp nftables.conf /etc/ 

# Create user 
useradd svcpassword

# Configure user daemon
mkdir -p /home/svcpassword/.config/systemd/user/
cp services/* /home/svcpassword/.config/systemd/user/
chown -R svcpassword:svcpassword /home/svcpassword/.config/

# Set service password
mkdir /home/svcpassword/svcpassword
cp -r active_directory /home/svcpassword/svcpassword/
cp -r passbolt /home/svcpassword/svcpassword
cp main.py /home/svcpassword/svcpassword
chown -R svcpassword:svcpassword /home/svcpassword/

# Install dependencies
su - svcpassword
pip3 install passboltapi gnugpg lap3 1>/dev/null
systemctl --user enable --now svcpassword.timer
systemctl --user enable --now svcpassword.service
exit

# Start all
systemctl enable --now nftables



