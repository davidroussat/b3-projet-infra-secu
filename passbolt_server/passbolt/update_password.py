import passboltapi
import sys
import gnupg
from . import Config
import logging

gpg = gnupg.GPG(gnupghome='/home/svcpassword/.gnupg')

passbolt = passboltapi.PassboltAPI(config_path="/home/svcpassword/svcpassword/passbolt/config.ini")

def get_ids_passwords():
    id_passwords = {}
    try:
        for password_id in passbolt.get("/resources.json")['body']:
            id_passwords[password_id['id']]= password_id['name']
        return id_passwords
    except (Exception,) as e:
        logging.error(f'Error in get_ids_passwords: {e}')


def get_admin_ids():
    admin_ids = {}
    try:     
        for admin in passbolt.get("/share/search-aros.json")['body']:
            admin_ids[admin['gpgkey']['fingerprint']] = admin['profile']['user_id']
        
        return admin_ids
    except (Exception,) as e:
        logging.error(f'Error in get_ids_passwords: {e}')


def _get_passwords_by_id(server_name):
    passwords = {}
    ids = get_ids_passwords()
    try:
        for id_passwd, name in ids.items():
            if name == server_name:    
                user_id = passbolt.get(f"/secrets/resource/{id_passwd}.json")['body']['user_id']
                psswd =  passbolt.get(f"/secrets/resource/{id_passwd}.json")['body']['data']
                passwords[psswd] = name
        return passwords
    except (Exception,) as e:
        logging.error(f'Error in _get_passwords_by_id: {e}')

def _decrypt_old_password(password):
    try:
        return gpg.decrypt(password)
    except(Exception,) as e: 
        logging.error(e)

def encrypt_data(newpassword, recipient, fingerprint, passphrase):
    encrypted_password = gpg.encrypt(newpassword, recipient, sign=fingerprint, passphrase=passphrase)
    human_password = str(encrypted_password)
    return human_password
    

def _set_secrets_data(newpassword):
    ids = get_admin_ids()
    secrets = []

    try:
        for fingerprint, admin_id in ids.items():
            if fingerprint == Config.ADMIN_FINGERPRINT:
                encrypted_data = encrypt_data(newpassword, Config.ADMIN_RECIPIENT, Config.ADMIN_FINGERPRINT, Config.ADMIN_PASSWORD)
                secrets.append({"user_id": admin_id, "data": encrypted_data})
            else: 
                encrypted_data = encrypt_data(newpassword, Config.SVC_RECIPIENT, Config.SVC_FINGERPRINT, Config.SVC_PASSWORD)
                secrets.append({"user_id": admin_id, "data": encrypted_data})
        return secrets
    except(Exception,) as e:
        logging.error(f'Error in _set_secrets_data function : {e}')

    return secrets


def update_password_by_server(server_name, newpassword):
    all_passwords = get_ids_passwords()
    secrets_tab = _set_secrets_data(newpassword)
    passwd_encrypted = _get_passwords_by_id(server_name) 
    
    try:
        for id_password, name in all_passwords.items():
            datas = {'name': name, 'description': ' ', 'secrets': secrets_tab}
            if name == server_name:
                try:
                    passbolt.put(f'/resources/{id_password}.json', datas)
                    logging.info(f"The password on {server_name} succesfully updated on Passbolt!\n")
                except (Exception,) as e:
                    logging.error(f'Error in put password: {e}')
        #return old_password
    except (Exception,) as e:
        logging.error(f'Error in update_password_by_name: {e}')
