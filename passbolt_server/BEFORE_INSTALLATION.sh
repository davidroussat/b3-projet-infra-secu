#!/bin/bash

set -euo pipefail
IFS=$'\n\t'

# Step-cli
wget https://dl.step.sm/gh-release/cli/docs-cli-install/v0.23.4/step-cli_0.23.4_amd64.deb
dpkg -i step-cli_0.23.4_amd64.deb
