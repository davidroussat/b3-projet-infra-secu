from active_directory.ad_password import change_ad_passwords
from passbolt.update_password import update_password_by_server
import random
import string
import logging
from logging.handlers import RotatingFileHandler
import os

LOG_FILE = "/home/svcpassword/svcpassword/svcpass.log"


def random_password():
    characters = string.ascii_letters + string.digits + string.punctuation
    password = ''.join(random.choice(characters) for i in range(18))

    return password

def set_password(server_name):
    new_password = random_password()

    if server_name =="pki":
        change_ad_passwords('matthias', new_password)

    elif server_name == "passbolt":
        change_ad_passwords('arnaud', new_password)
 
    elif server_name == "bastion":
        change_ad_passwords('david', new_password)

    else:
        change_ad_passwords('theo', new_password)


def setup_logging() -> None:
    log_rotate_handler = RotatingFileHandler(os.path.join(LOG_FILE), maxBytes=10000000, backupCount=10)
    logging.basicConfig(format="%(asctime)s %(levelname)s: %(message)s", level="INFO", handlers=[log_rotate_handler])


if __name__ == '__main__':
    servers = [ "pki", "passbolt", "bastion", "server_mail"]
    setup_logging() 
    logging.info("-------Program started--------\n")
    for server in servers:
        set_password(server)
