import ldap3
from ldap3 import *
from . import Config
import logging

def connexion_ldaps_serveur():
    tls = Tls(local_private_key_file = '/home/svcpassword/svcpassword/active_directory/ldaps_key.pem', local_certificate_file = '/home/svcpassword/svcpassword/active_directory/ldaps_crt.pem', ca_certs_file='/home/svcpassword/svcpassword/active_directory/DC.crt')
    serveur = Server(Config.LDAPS_SERVEUR, port = 636, use_ssl = True, tls = tls)
    
    try:
        connected = Connection(serveur, user=Config.SVC_USER, password=Config.SVC_PASSWORD)
        return connected
    except:
        logging.info(f'Error in connexion_ldpas_serveur: {connected.result}')

def change_ad_passwords(user_name, new_password, old_password=None):
    connexion = connexion_ldaps_serveur()
    user_dn=f'CN={user_name},OU=Utilisateurs,DC=projet,DC=lab'
    connexion.bind()
    
    try:
        success = ldap3.extend.microsoft.modifyPassword.ad_modify_password(connexion, user_dn, new_password, old_password=None)
        if success == True:
            logging.info(f"{user_name}'s active dicrectry password changed with success\n")
        else:
            logging.info(f'The {old_password} can not be changed')
    
    except(Exception,) as e:
        logging.error(f'Error in change_ad_passwords: {e}')



