# Il est primordial d'effectuer ces étapes dans l'ordre:

1 - Lancer le script BEFORE_INSTALLATION.sh en tant que root

2 - Sur le server PKI, lancer la commande suivante afin de récupérer le "CA FINGERPRINT"

	step certificate fingerprint $(step path)/certs/root_ca.crt

3 - Etablir une relation de confiance avec la PKI (en tant que NON root)

	step ca bootstrap --ca-url https://pki.projet.lab:8443 --fingerprint [CA fingerprint]

4 - Demander l'obtention des certificats en lancant les commandes suivantes:

	step ca certificate vpn.crt vpn.key

NB: A chaque demande de certficat, un mot passe sera nécessaire. Contactez-moi afin que je vous le fournisse.

5 - Installer la root_ca dans le Truststore:

	step certificate install $(step path)/certs/root_ca.crt

6 - Lancer le script installation.sh

7- Pour joindre le serveur au domain:
	a- Taper la commande: 
		realm join -U Administrator projet.lab
	Un mot de passe vous sera demander:	
		"azertyuiop"
	
	b- Executer le script joind_ad.sh
