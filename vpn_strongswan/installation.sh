#!/bin/bash

set -euo pipefail
IFS=$'\n\t'

# Network configuration
nmcli conn add con-name privee ifname enp0s8 ipv4.address 10.7.0.21/24 ipv4.gateway 10.7.0.254  ipv4.method manual type ethernet connection.autoconnect yes

nmcli conn reload
nmcli conn up enp0s8

nmcli conn add con-name vpn ifname enp0s9 ipv4.address 192.168.184.10/24 ipv4.gateway 192.168.184.254 ipv4.method manual type ethernet connection.autoconnect yes

nmcli conn reload
nmcli conn up enp0s9

# Hostname configuration
hostnamectl set-hostname vpn.projet.lab

# Installation 
dnf update -y 1>/dev/null
dnf install epel-release -y 1>/dev/null
dnf install realmd sssd oddjob oddjob-mkhomedir adcli samba-common samba-common-tools krb5-workstation authselect-compat strongswan -y 1>/dev/null

cp swanctl.conf /etc/strongswan/swanctl/swanctl.conf
mv /root/vpn.crt /etc/strongswan/swanctl/x509/
mv /root/vpn.key /etc/strongswan/swanctl/private/
cp /root/.step/certs/root_ca.crt /etc/strongswan/swanctl/x509ca/

authselect select sssd
authselect select sssd with-mkhomedir

systemctl enable --now strongswan

echo "Strongswan configured to authenticate with certificate! Adapt with your environment and request certificate!"

# Firewall configurations
rm /etc/nftables/*.nft 
cp rules.nft /etc/nftables/

start_fw=$(systemctl enable --now nftables)

if [[ ! $start_fw -eq 0 ]]; then
	echo "Something wrong with nftables"

else 
	echo "FW configured with success!"
fi


