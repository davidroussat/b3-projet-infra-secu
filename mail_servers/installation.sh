#!/bin/bash

# PKI Installation by David R.

set -euo pipefail
IFS=$'\n\t'

# Network configuration
nmcli conn add con-name privee ifname enp0s8 ipv4.address 10.7.0.20/24 ipv4.gateway 10.7.0.254  ipv4.method manual type ethernet connection.autoconnect yes

nmcli conn reload
nmcli conn up enp0s8

# Hostname configuration
hostnamectl set-hostname mail.projet.lab

apt update -y 1>/dev/null
tee -a /etc/apt/sources.list <<EOF
deb http://us.archive.ubuntu.com/ubuntu/ bionic universe
deb http://us.archive.ubuntu.com/ubuntu/ bionic-updates universe
EOF
apt -y install realmd libnss-sss libpam-sss sssd sssd-tools adcli samba-common-bin oddjob oddjob-mkhomedir packagekit


# Postfix installation
apt install postfix -y 1>/dev/null
cp postfix/* /etc/postfix/

# Dovecot installation
apt install dovecot -y 1>/dev/null
cp dovecot/dovecot.conf /etc/dovecot/
cp dovecot/10-auth.conf /etc/dovecot/conf.d/
cp dovecot/10-mail.conf /etc/dovecot/conf.d/
cp dovecot/10-master.conf /etc/dovecot/conf.d/
cp dovecot/10-ssl.conf /etc/dovecot/conf.d/ 

# Disable UFW firewall
systemctl disable ufw.service
systemctl stop ufw.service
systemctl mask ufw.service

# Nftables installation
apt install nftables -y 1>/dev/null
cp nftables.conf /etc/ 

# Create services and timers
cp services/* /etc/systemd/system/

# Start all
systemctl enable --now dovecot.service
systemctl enable --now postfix.service
systemctl enable --now cert-renew-dovecot.service
systemctl enable --now cert-renew-postfix.service
systemctl enable --now cert-renewer-dovecot.timer
systemctl enable --now cert-renewer-postfix.timer
systemctl enable --now nftables.service
