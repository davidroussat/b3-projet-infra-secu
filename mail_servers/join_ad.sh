#!/bin/bash

# Créer dynamiquement un répertoire pour les utilisateurs du domaine:
bash -c "cat > /usr/share/pam-configs/mkhomedir" <<EOF
Name: activate mkhomedir
Default: yes
Priority: 900
Session-Type: Additional
Session:
	required                        pam_mkhomedir.so umask=0022 skel=/etc/skel
EOF

# Update pam:
pam-auth-update
systemctl restart sssd

# Allow Arnaud connexion
realm permit theo@projet.lab
