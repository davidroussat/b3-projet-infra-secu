$comp_certificate = "C:\Users\Administrator\Desktop\vpn.pfx"
$password = "secret"
$ca_cert = "C:\Users\Administrator\Desktop\root_ca.crt"


# Import certificate from PKI Linux
Import-PfxCertificate -FilePath $comp_certificate -CertStoreLocation Cert:\LocalMachine\My\ -Password $password

# Import root_ca from PKI Linux
Import-Certificate -FilePath $ca_cert -CertStoreLocation Cert:\LocalMachine\Root\

# VPN configuration
Add-VpnConnection -Name "test" -ServerAddress "vpn.projet.lab" -TunnelType Ikev2 -EncryptionLevel "Maximum" -AuthenticationMethod MachineCertificate -SplitTunneling $True -PassThru