# Installation
Add-WindowsFeature Adcs-Cert-Authority -IncludeManagementTools 

# Configuration
Install-AdcsCertificationAuthority `
  -CAType EnterpriseRootCa `
  -CryptoProviderName "RSA#Microsoft Software Key Storage Provider" `
  -KeyLength 4096 `
  -HashAlgorithmName SHA512 `
  -ValidityPeriod Years `
  -ValidityPeriodUnits 3

# Export
certutil -ca.cert C:\Users\Administrator\Desktop\DC.crt

# Create new certificate to Ldaps
Get-Certificate -Template "Kerberos" -DnsName "passbolt.projet.lab" -CertStoreLocation "cert\LocalMachine\My"

# Export certificate + key
$a = Get-ChildItem -Path Cert:\LocalMachine\My

$params = @{
    Cert = $a[1]
    FilePath = 'C:\Users\Administrator\Desktop\ldaps.pfx'
}
Export-PfxCertificate @params
