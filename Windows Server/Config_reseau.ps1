# Set ip
New-NetIPAddress –InterfaceIndex 6 –IPAddress 10.7.0.10 –PrefixLength 24 –DefaultGateway 10.7.0.254


# Set DNS
Set-DnsClientServerAddress -InterfaceIndex 6 -ServerAddresses 10.7.0.10
