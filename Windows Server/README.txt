Lancer les scripts dans l'ordre suivant:

    1- Config_reseau.ps1
    2- Ad_deploiement.ps1
    3- AD_Config.ps1
    4- Install adcs_rôle.ps1

Transférer ensuite le certificat C:\Users\Administrator\Desktop\ldaps.pfx sur le serveur passbolt à l'endroit "/home/david/"

Récupérer ensuite la root_ca.crt puis le vpn.pfx généré depuis le serveur de pki.projet.lab.

Terminer par lancer le dernier script VPN_installation.ps1
