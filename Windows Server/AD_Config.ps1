# Création de l'organisation
Import-Module ActiveDirectory

New-ADOrganizationalUnit -Name Administrateur_applicatif -Path "DC=PROJET,DC=LAB"

# Création de groupe
New-ADGroup -GroupScope Global `
	-Name Applicatif -DisplayName Applictif `
	-GroupCategory Security -PassThru `
	-Path "OU=Administrateur_applicatif,DC=projet,DC=lab"

# Création des utilisateurs
New-ADUser -Name matthias `
	-AccountPassword (ConvertTo-SecureString -AsPlainText Azerty123 -Force) `
	-CannotChangePassword $False `
	-ChangePasswordAtLogon $True `
	-PasswordNeverExpires $False `
	-PasswordNotRequired $Fasle `
	-Path "OU=Administrateur_applicatif,DC=projet,DC=lab" `
	-Enabled $True

New-ADUser -Name theo `
	-AccountPassword (ConvertTo-SecureString -AsPlainText Azerty123 -Force) `
	-CannotChangePassword $False `
	-ChangePasswordAtLogon $True `
	-PasswordNeverExpires $False `
	-PasswordNotRequired $Fasle `
	-Path "OU=Administrateur_applicatif,DC=projet,DC=lab" `
	-Enabled $True

New-ADUser -Name david `
	-AccountPassword (ConvertTo-SecureString -AsPlainText Azerty123 -Force) `
	-CannotChangePassword $False `
	-ChangePasswordAtLogon $True `
	-PasswordNeverExpires $False `
	-PasswordNotRequired $Fasle `
	-Path "OU=Administrateur_applicatif,DC=projet,DC=lab" `
	-Enabled $True

New-ADUser -Name arnaud `
	-AccountPassword (ConvertTo-SecureString -AsPlainText Azerty123 -Force) `
	-CannotChangePassword $False `
	-ChangePasswordAtLogon $True `
	-PasswordNeverExpires $False `
	-PasswordNotRequired $Fasle `
	-Path "OU=Administrateur_applicatif,DC=projet,DC=lab" `
	-Enabled $True


# Ajout des utilisateurs au groupe
Add-ADGroupMember -Identity Applicatif -Members arnaud,david,theo,matthias
