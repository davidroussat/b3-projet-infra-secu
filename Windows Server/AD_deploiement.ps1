Import-Module ADDSDeployment

$Password="xxxxx"
$SafePassword=ConvertTo-SecureString $Password -AsPlaintext -Force

Install-ADDSForest -DomainName "projet.lab" -DatabasePath "C:\Windows\NTDS" `
	-DomainMode "WinThreshold" -DomainNetbiosName "PROJET" `
	-ForestMode "WinThreshold" -InstallDns -LogPath "C:\Windows\NTDS" `
	-SysvolPath "C:\Windows\SYSVOL" -SafeModeAdministratorPassword $SafePassword `
	-Force   
