#!/bin/bash

# PKI Installation by David R.

set -euo pipefail
IFS=$'\n\t'

# Network configuration
nmcli conn add con-name privee ifname enp0s8 ipv4.address 10.7.0.12/24 ipv4.gateway 10.7.0.254  ipv4.method manual type ethernet connection.autoconnect yes

nmcli conn reload
nmcli conn up enp0s8

# Hostname configuration
hostnamectl set-hostname bastion.projet.lab

# Package installations
apt update -y 1>/dev/null

tee -a /etc/apt/sources.list <<EOF
deb http://us.archive.ubuntu.com/ubuntu/ bionic universe
deb http://us.archive.ubuntu.com/ubuntu/ bionic-updates universe
EOF
apt -y install realmd libnss-sss libpam-sss sssd sssd-tools adcli samba-common-bin oddjob oddjob-mkhomedir packagekit 1>/dev/null

# Create user service
mkdir -p /home/david/.config/systemd/user/
cp services/* /home/david/.config/systemd/user/
chown -R david:david /home/david/.config/


# Disable UFW firewall
systemctl disable ufw.service
systemctl stop ufw.service
systemctl mask ufw.service

# Nftables installation
apt install nftables -y 1>/dev/null
cp nftables.conf /etc/ 


# Start all
systemctl enable --now nftables

# Switch user
su - david
systemctl --user daemon-reload
systemctl --user start step.service
systemctl --user start step.timer
systemctl --user enable step.service
systemctl --user enable step.timer

